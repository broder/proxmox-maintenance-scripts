#!/bin/bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

for CTID in $(pct list | tail -n+2 | grep running | cut -d' ' -f1); do
  printf "Apt autoremoving container %s\n" $CTID
  pct exec $CTID -- bash -c 'apt -qq -y autoremove' 2>&1
  printf "\n"
done
