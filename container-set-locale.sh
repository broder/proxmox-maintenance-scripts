#!/bin/bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

for CTID in $(pct list | tail -n+2 | grep running | cut -d' ' -f1); do
  printf "Get info about container %s\n" $CTID
  pct exec $CTID -- bash -c 'locale-gen en_US.UTF-8' 2>&1
  printf "\n"
done
