#!/bin/bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

for CTID in $(pct list | tail -n+2 | grep running | cut -d' ' -f1); do
  printf "Upgrading container %s\n" $CTID
  pct exec $CTID -- bash -c 'apt -qq -y upgrade' 2>&1
  printf "\n"
done
