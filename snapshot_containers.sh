#!/bin/bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

for CTID in $(pct list | tail -n+2 | grep running | cut -d' ' -f1); do
  printf "Snapshotting container %s\n" $CTID
  pct snapshot $CTID Upgrades_`date +"%m_%d_%y__%H_%M"`
  printf "\n"
done
