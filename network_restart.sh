#!/bin/bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

echo "Restarting networing interface and reattach virtual adapters"

echo "These adapters are attached right now:"
brctl show vmbr0

echo "--------------------"

# Store the list of interfaces connected to the virtual bridge
bridge_interfaces=($(brctl show vmbr0 | awk '{print $(NF)}' | sed 1d))

# Restarts the network adapter. This causes all active port mappings from nat
# to the virtual network adapter to be lost.
echo "Restarting network..."
iptables -t nat --flush
systemctl restart networking
echo "Network restart complete."
echo "--------------------"

for IFACE in ${bridge_interfaces[*]}; do
  printf "Attaching interface %s\n" $IFACE
  brctl addif vmbr0 $IFACE
done

echo "--------------------"

echo "All interfaces were added. This should now look like in the beginning:"
brctl show vmbr0
